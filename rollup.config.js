import commonjs from 'rollup-plugin-commonjs'

module.exports = {
  input: './src/main.js',
  external: [
    'express',
    'express-validator/check',
    'express-validator/filter',
    'chalk',
    'crypto',
    'moment',
    'uuid/v4',
    'pg',
    'dotenv',
  ],
  output: {
    file: './build/vendor.js',
    format: 'cjs',
  },
  plugins: [commonjs()],
}
