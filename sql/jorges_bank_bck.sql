--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3
-- Dumped by pg_dump version 11.3

-- Started on 2019-06-02 13:32:44

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 197 (class 1259 OID 16502)
-- Name: freed_money; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.freed_money (
    id character varying(36) NOT NULL,
    hash character varying(64) NOT NULL,
    value integer NOT NULL
);


ALTER TABLE public.freed_money OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 16478)
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id character varying(36) NOT NULL,
    hash character varying(64) NOT NULL,
    money integer DEFAULT 0 NOT NULL,
    created_at timestamp(4) with time zone NOT NULL
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- TOC entry 2693 (class 2606 OID 16567)
-- Name: freed_money freed_money_hash_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.freed_money
    ADD CONSTRAINT freed_money_hash_unique UNIQUE (hash);


--
-- TOC entry 2695 (class 2606 OID 16560)
-- Name: freed_money freed_money_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.freed_money
    ADD CONSTRAINT freed_money_pkey PRIMARY KEY (id);


--
-- TOC entry 2689 (class 2606 OID 16581)
-- Name: user user_hash_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_hash_unique UNIQUE (hash);


--
-- TOC entry 2691 (class 2606 OID 16574)
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


-- Completed on 2019-06-02 13:32:44

--
-- PostgreSQL database dump complete
--

