import chalk from 'chalk'
import { createHash } from 'crypto'

export const print = {
  info: message => {
    console.info(chalk.blue('[INFO] ') + message)
  },
  error: message => {
    console.error(chalk.red('[ERROR] ') + message)
  },
  warn: message => {
    console.warn(chalk.yellow('[WARN] ') + message)
  },
  success: message => {
    console.log(chalk.green('[SUCCESS] ') + message)
  },
  log: message => {
    console.log('[LOG] ' + message)
  },
}

export function jsend_resonse(status, values) {
  if (status === 'success' || status === 'fail') {
    return {
      status: status,
      data: { ...values },
    }
  } else if (status === 'error') {
    return {
      status: status,
      message: values,
    }
  }
}

export function verify_hash(pub, secret, hash) {
  const sha256 = createHash('sha256')
  sha256.update(pub + secret)
  if (sha256.digest('hex') === hash) {
    return true
  } else {
    return false
  }
}
