import { config } from 'dotenv'
import { Pool } from 'pg'

config()

const pool = new Pool({
  connectionString: process.env.DATABASE_URL,
})

export default {
  query: (text, params, callback) => {
    return pool.query(text, params, callback)
  },
}
