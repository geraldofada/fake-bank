import { config } from 'dotenv'
import express from 'express'

import users from './routes/users'
import payments from './routes/payments'
import { print, jsend_resonse } from './utils'

config()

const app = express()

app.use(express.json())
app.use('/users', users)
app.use('/payments', payments)

app.use('*', (req, res) => {
  const response = jsend_resonse('fail', null)
  return res.status(404).send(response)
})

app.listen(process.env.PORT, () => {
  print.info('Listening on port 3000')
})
