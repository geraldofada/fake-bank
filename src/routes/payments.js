import { Router } from 'express'
import { check, validationResult } from 'express-validator/check'
import { sanitizeBody } from 'express-validator/filter'

import {
  get_money,
  make_payment,
  release_money,
  deposit_money,
} from '../models/payments'

import { jsend_resonse, print } from '../utils'

const router = Router()

router.get('/money/:id', (req, res) => {
  const id = req.params.id

  get_money(id)
    .then(result => {
      if (result !== null) {
        const response = jsend_resonse('success', result)
        return res.send(response)
      } else {
        const response = jsend_resonse('fail', {
          message: "Money isn't valid",
        })
        res.status(400)
        return res.send(response)
      }
    })
    .catch(err => {
      const response = jsend_resonse('error', 'An internal error ocurred')
      print.error(err)
      res.status(500)
      return res.send(response)
    })
})

router.post(
  '/',
  [
    check('sender_id')
      .isUUID(4)
      .withMessage('Must be and uuidv4'),
    check('sender_password')
      .exists({ checkFalsy: true })
      .withMessage('Please provide a password'),
    check('receiver_id')
      .isUUID(4)
      .withMessage('Must be and uuidv4'),
    check('amount')
      .isInt()
      .withMessage('Must be a integer'),

    sanitizeBody('sender_password').toString(),
    sanitizeBody('amount').toInt(),
  ],
  (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      const response = jsend_resonse('fail', { errors: errors.array() })
      res.status(400)
      return res.send(response)
    }

    const sender_id = req.body.sender_id
    const sender_password = req.body.sender_password
    const receiver_id = req.body.receiver_id
    const amount = req.body.amount

    make_payment(sender_id, sender_password, receiver_id, amount)
      .then(result => {
        if (result !== null) {
          const response = jsend_resonse('success', result)
          return res.send(response)
        } else {
          const response = jsend_resonse('fail', {
            message:
              "Wrong password, users doesn't exists or sender doesn't have enough money",
          })
          res.status(400)
          return res.send(response)
        }
      })
      .catch(err => {
        const response = jsend_resonse('error', 'An internal error ocurred')
        print.error(err)
        res.status(500)
        return res.send(response)
      })
  }
)

router.post(
  '/release/money',
  [
    check('id')
      .isUUID(4)
      .withMessage('Must be and uuidv4'),
    check('password')
      .exists({ checkFalsy: true })
      .withMessage('Please provide a password'),
    check('amount')
      .isInt()
      .withMessage('Must be a integer'),

    sanitizeBody('password').toString(),
    sanitizeBody('amount').toInt(),
  ],
  (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      const response = jsend_resonse('fail', { errors: errors.array() })
      res.status(400)
      return res.send(response)
    }

    const id = req.body.id
    const password = req.body.password
    const amount = req.body.amount

    release_money(id, password, amount)
      .then(result => {
        if (result !== null) {
          const response = jsend_resonse('success', result)
          return res.send(response)
        } else {
          const response = jsend_resonse('fail', {
            message:
              "Wrong password, user doesn't exist or doesn't have enough money",
          })
          res.status(400)
          return res.send(response)
        }
      })
      .catch(err => {
        const response = jsend_resonse('error', 'An internal error ocurred')
        print.error(err)
        res.status(500)
        return res.send(response)
      })
  }
)

router.post(
  '/deposit/money',
  [
    check('pub_id')
      .isUUID(4)
      .withMessage('Must be and uuidv4'),
    check('secret_id')
      .isUUID(4)
      .withMessage('Must be and uuidv4'),
    check('user_id')
      .isUUID(4)
      .withMessage('Must be and uuidv4'),
  ],
  (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      const response = jsend_resonse('fail', { errors: errors.array() })
      res.status(400)
      return res.send(response)
    }

    const pub_id = req.body.pub_id
    const secret_id = req.body.secret_id
    const user_id = req.body.user_id

    deposit_money(pub_id, secret_id, user_id)
      .then(result => {
        if (result !== null) {
          const response = jsend_resonse('success', result)
          return res.send(response)
        } else {
          const response = jsend_resonse('fail', {
            message: "Money isn't valid or user doesn't exist",
          })
          res.status(400)
          return res.send(response)
        }
      })
      .catch(err => {
        const response = jsend_resonse('error', 'An internal error ocurred')
        print.error(err)
        res.status(500)
        return res.send(response)
      })
  }
)

export default router
