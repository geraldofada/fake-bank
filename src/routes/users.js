import { Router } from 'express'
import { check, validationResult } from 'express-validator/check'
import { sanitizeBody } from 'express-validator/filter'

import { jsend_resonse, print } from '../utils'

import { get_user, get_user_details, create_user } from '../models/users'

const router = Router()

router.get('/verify/:id', (req, res) => {
  const id = req.params.id
  get_user(id)
    .then(result => {
      if (result !== null) {
        const response = jsend_resonse('success', result)
        return res.send(response)
      } else {
        const response = jsend_resonse('fail', result)
        res.status(400)
        return res.send(response)
      }
    })
    .catch(err => {
      const response = jsend_resonse('error', 'An interal error ocurred')
      print.error(err)
      res.status(500)
      return res.send(response)
    })
})

router.post(
  '/details/:id',
  [
    check('password')
      .exists({ checkFalsy: true })
      .withMessage('Please provide a password'),

    sanitizeBody('password').toString(),
  ],
  (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      const response = jsend_resonse('fail', { errors: errors.array() })
      res.status(400)
      return res.send(response)
    }

    const id = req.params.id
    const password = req.body.password
    get_user_details(id, password)
      .then(result => {
        if (result !== null) {
          const response = jsend_resonse('success', result)
          return res.send(response)
        } else {
          const response = jsend_resonse('fail', result)
          res.status(400)
          return res.send(response)
        }
      })
      .catch(err => {
        const response = jsend_resonse('error', 'An interal error ocurred')
        print.error(err)
        res.status(500)
        return res.send(response)
      })
  }
)

router.post(
  '/',
  [
    check('password')
      .exists({ checkFalsy: true })
      .withMessage('Please provide a password'),

    sanitizeBody('password').toString(),
  ],
  (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      const response = jsend_resonse('fail', { errors: errors.array() })
      res.status(400)
      return res.send(response)
    }

    const password = req.body.password
    create_user(password)
      .then(result => {
        const response = jsend_resonse('success', result)
        print.success('User created with id: ' + result.post.id)
        return res.send(response)
      })
      .catch(err => {
        const response = jsend_resonse('error', 'An interal error ocurred')
        print.error(err)
        res.status(500)
        return res.send(response)
      })
  }
)

export default router
