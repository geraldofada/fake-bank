import { createHash } from 'crypto'
import uuidv4 from 'uuid/v4'
import { verify_hash } from '../utils'
import db from '../db'

export async function get_money(pub_id) {
  const sql_freed_money = 'SELECT id, value FROM "freed_money" WHERE id=$1'

  const freed_money = await db.query(sql_freed_money, [pub_id])
  if (freed_money.rowCount > 0) {
    return {
      get: {
        id: pub_id,
        value: freed_money.rows[0].value,
      },
    }
  }

  return null
}

export async function make_payment(
  sender_id,
  sender_password,
  receiver_id,
  amount
) {
  const sql_user = 'SELECT hash, money from "user" WHERE id=$1'
  const sql_update_money = 'UPDATE "user" SET money=$1 WHERE id=$2'

  const sender = await db.query(sql_user, [sender_id])
  const receiver = await db.query(sql_user, [receiver_id])

  if (receiver.rowCount > 0 && sender.rowCount > 0) {
    if (verify_hash(sender_id, sender_password, sender.rows[0].hash)) {
      if (sender.rows[0].money >= amount) {
        const sender_money = sender.rows[0].money - amount
        const receiver_money = receiver.rows[0].money + amount

        await db.query(sql_update_money, [sender_money, sender_id])
        await db.query(sql_update_money, [receiver_money, receiver_id])

        return {
          post: {
            sender_id: sender_id,
            sender_money: sender_money,
            receiver_id: receiver_id,
            amount_sended: amount,
          },
        }
      }
    }
  }

  return null
}

export async function release_money(id, password, amount) {
  const sql_user = 'SELECT money, hash FROM "user" WHERE id=$1'
  const sql_update_money = 'UPDATE "user" SET money=$1 WHERE id=$2'
  const sql_insert_money =
    'INSERT INTO "freed_money" (id, hash, value) VALUES ($1, $2, $3)'

  const user = await db.query(sql_user, [id])

  if (user.rowCount > 0) {
    if (verify_hash(id, password, user.rows[0].hash)) {
      if (user.rows[0].money >= amount) {
        const user_new_money = user.rows[0].money - amount
        await db.query(sql_update_money, [user_new_money, id])

        const freed_money_pubid = uuidv4()
        const freed_money_localid = uuidv4()
        const freed_money_hash = createHash('sha256')
          .update(freed_money_pubid + freed_money_localid)
          .digest('hex')

        await db.query(sql_insert_money, [
          freed_money_pubid,
          freed_money_hash,
          amount,
        ])

        return {
          post: {
            user_id: id,
            amount: amount,
            money_pub: freed_money_pubid,
            money_secret: freed_money_localid,
          },
        }
      }
    }
  }

  return null
}

export async function deposit_money(pub_id, secret_id, user_id) {
  const sql_freed_money = 'SELECT hash, value FROM "freed_money" WHERE id=$1'
  const sql_delete_freed_money = 'DELETE FROM "freed_money" WHERE id=$1'
  const sql_user = 'SELECT money from "user" WHERE id=$1'
  const sql_update_user = 'UPDATE "user" SET money=$1 WHERE id=$2'

  const freed_money = await db.query(sql_freed_money, [pub_id])
  const user = await db.query(sql_user, [user_id])

  if (freed_money.rowCount > 0 && user.rowCount > 0) {
    if (verify_hash(pub_id, secret_id, freed_money.rows[0].hash)) {
      const user_new_money = user.rows[0].money + freed_money.rows[0].value

      await db.query(sql_update_user, [user_new_money, user_id])
      await db.query(sql_delete_freed_money, [pub_id])

      return {
        post: {
          user_id: user_id,
          amount: freed_money.rows[0].value,
        },
      }
    }
  }

  return null
}
