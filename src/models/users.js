import { createHash } from 'crypto'
import moment from 'moment'
import uuidv4 from 'uuid/v4'

import db from '../db'
import { verify_hash } from '../utils'

export async function create_user(password) {
  const sql_user =
    'INSERT INTO "user" (id, hash, money, created_at) VALUES ($1, $2, $3, $4)'
  const id = uuidv4()
  const user_hash = createHash('sha256')
    .update(id + password)
    .digest('hex')
  const money = 0
  const today = moment().format()

  await db.query(sql_user, [id, user_hash, money, today])

  return {
    post: {
      id: id,
      money: money,
      created_at: today,
    },
  }
}

export async function get_user(id) {
  const sql_id = 'SELECT id FROM "user" WHERE id=$1'

  const user_id = await db.query(sql_id, [id])
  if (user_id.rowCount > 0) {
    return {
      get: {
        id: user_id.rows[0].id,
      },
    }
  } else {
    return null
  }
}

export async function get_user_details(id, password) {
  const sql_user = 'SELECT hash, money, created_at FROM "user" WHERE id=$1'

  const user = await db.query(sql_user, [id])

  if (user.rowCount > 0) {
    if (verify_hash(id, password, user.rows[0].hash)) {
      return {
        get: {
          id: id,
          money: user.rows[0].money,
          created_at: moment(user.rows[0].created_at).format(),
        },
      }
    }
  }

  return null
}
