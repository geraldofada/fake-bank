import { verify_hash } from '../src/utils'
import { createHash } from 'crypto'

describe('verify_hash(pub, secret, hash)', () => {
  it('should return true if hash equals to pub+secret', () => {
    const pub = 'pub'
    const secret = 'secret'
    const hash = createHash('sha256')
      .update(pub + secret)
      .digest('hex')
    expect(verify_hash(pub, secret, hash)).toBe(true)
  })

  it("should return false if hash isn't equals to pub+secret", () => {
    const pub = 'pub'
    const secret = 'secret'
    const hash = createHash('sha256')
      .update(pub + secret + 'wrong_value')
      .digest('hex')
    expect(verify_hash(pub, secret, hash)).toBe(false)
  })
})
